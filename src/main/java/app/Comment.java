package app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a comment submitted by a user
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comment {
    /**
     * Content of the comment
     */
    private final String content;

    public Comment(
            @JsonProperty("content")  final String content
    ) {
        this.content = content;
    }

    public InternalComment toInternalComment(long id, boolean approved) {
        return new InternalComment(id, content, approved);
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return this.content;
    }
}
