package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class CommentController {

    private AtomicLong nextIdCounter = new AtomicLong(1);

    /**
     * A sudo storage to save comments
     */
    private ArrayList<InternalComment> storage = new ArrayList<InternalComment>();

    private long nextId() {
        return nextIdCounter.getAndIncrement();
    }

    /**
     * Gets JSON from POST request body of users and
     * stores.
     * @return comment content
     */
    @PostMapping("/submit")
    public String newComment(@RequestBody Comment comment) {
        this.storage.add(comment.toInternalComment(nextId(), false));
        return "Your comment '" + comment + "' has been successfully stored";
    }
}
