package app;

/**
 * Represents a comment submitted by a user
 */
public class InternalComment extends Comment {
    /**
     * Comment identifier
     */
    private final long id;

    /**
     * if comment is approved or not by moderator
     */
    private final boolean approved;

    public InternalComment(final long id, final String content, final boolean approved) {
        super(content);
        this.id = id;
        this.approved = approved;
    }

    public long getId() {
        return id;
    }

    public boolean getApproved() {
        return approved;
    }
}
